[extern isr_handler]
[extern irq_handler]

%macro ISR_CPU_ERR 1
isr%1:
	cli
	push byte %1
	jmp isr_common_stub
%endmacro

%macro ISR_CPU_NOERR 1
isr%1:
	cli
	push byte 0
	push byte %1
	jmp isr_common_stub
%endmacro

%macro IRQ 2
irq%1:
	cli
	push byte %1
	push byte %2
	jmp irq_common_stub
%endmacro

%macro PUSHALL 0
	push rax
	push rdx
	push rcx
	push rbx
	push rbp
	push rsi
	push rdi
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
%endmacro

%macro POPALL 0
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rdi
	pop rsi
	pop rbp
	pop rbx
	pop rcx
	pop rdx
	; rax to be popped manually
%endmacro

isr_common_stub:
	PUSHALL

	mov ax, ds
	push rax

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call isr_handler

	pop rax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	POPALL
	pop rax
	add rsp, 16

	iretq

irq_common_stub:
	PUSHALL

	mov ax, ds
	push rax

	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call irq_handler

	pop rax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	POPALL
	pop rax
	add rsp, 16

	iretq

global isr0
global isr1
global isr2
global isr3
global isr4
global isr5
global isr6
global isr7
global isr8
global isr9
global isr10
global isr11
global isr12
global isr13
global isr14
global isr15
global isr16
global isr17
global isr18
global isr19
global isr20
global isr21
global isr22
global isr23
global isr24
global isr25
global isr26
global isr27
global isr28
global isr29
global isr30
global isr31

global irq0
global irq1
global irq2
global irq3
global irq4
global irq5
global irq6
global irq7
global irq8
global irq9
global irq10
global irq11
global irq12
global irq13
global irq14
global irq15

ISR_CPU_NOERR 0
ISR_CPU_NOERR 1
ISR_CPU_NOERR 2
ISR_CPU_NOERR 3
ISR_CPU_NOERR 4
ISR_CPU_NOERR 5
ISR_CPU_NOERR 6
ISR_CPU_NOERR 7
ISR_CPU_ERR 8
ISR_CPU_NOERR 9
ISR_CPU_ERR 10
ISR_CPU_ERR 11
ISR_CPU_ERR 12
ISR_CPU_ERR 13
ISR_CPU_ERR 14
ISR_CPU_NOERR 15
ISR_CPU_NOERR 16
ISR_CPU_NOERR 17
ISR_CPU_NOERR 18
ISR_CPU_NOERR 19
ISR_CPU_NOERR 20
ISR_CPU_NOERR 21
ISR_CPU_NOERR 22
ISR_CPU_NOERR 23
ISR_CPU_NOERR 24
ISR_CPU_NOERR 25
ISR_CPU_NOERR 26
ISR_CPU_NOERR 27
ISR_CPU_NOERR 28
ISR_CPU_NOERR 29
ISR_CPU_NOERR 30
ISR_CPU_NOERR 31

IRQ 0, 32
IRQ 1, 33
IRQ 2, 34
IRQ 3, 35
IRQ 4, 36
IRQ 5, 37
IRQ 6, 38
IRQ 7, 39
IRQ 8, 40
IRQ 9, 41
IRQ 10, 42
IRQ 11, 43
IRQ 12, 44
IRQ 13, 45
IRQ 14, 46
IRQ 15, 47
