[ORG 0x7c00]
[BITS 16]

%define DISK_READ_FN 0x2
%define NUM_SECTORS 32
%define START_SECTOR 2
%define CYLINDER 0
%define HEAD 0
%define KERNEL_OFFSET 0x8000

%define PAGE_TABLES_START 0xa000
%define PAGE_TABLE_REM_WORDS 0x7ff
%define PML4_FIRST_WORD 0xb00f
%define PDP_FIRST_WORD 0xc00f
%define PD_FIRST_WORD 0x018f

%define PAE_AND_PGE_BITS 0b10100000
%define MSR_ADDR 0xC0000080
%define LONG_MODE_BIT 0x00000100
%define PAG_AND_PROT_BITS 0x80000001

%define STACK_BASE 0x90000

; Enable A20 via port 0x92
in al, 0x92
or al, 0x2
out 0x92, al

; Load kernel into memory
mov ah, DISK_READ_FN
mov al, NUM_SECTORS
mov ch, CYLINDER
mov cl, START_SECTOR
mov dh, HEAD
mov bx, KERNEL_OFFSET
int 0x13

; Build necessary page tables
xor bx, bx
mov es, bx
cld
mov di, PAGE_TABLES_START

mov ax, PML4_FIRST_WORD
stosw
xor ax, ax
mov cx, PAGE_TABLE_REM_WORDS
rep stosw

mov ax, PDP_FIRST_WORD
stosw
xor ax, ax
mov cx, PAGE_TABLE_REM_WORDS
rep stosw

mov ax, PD_FIRST_WORD
stosw
xor ax, ax
mov cx, PAGE_TABLE_REM_WORDS
rep stosw

; Enter Long Mode
mov eax, PAE_AND_PGE_BITS
mov cr4, eax

mov edx, PAGE_TABLES_START
mov cr3, edx

mov ecx, MSR_ADDR
rdmsr
or eax, LONG_MODE_BIT
wrmsr

mov ebx, cr0
or ebx, PAG_AND_PROT_BITS
mov cr0, ebx

; Load GDT
lgdt [gdt.pointer]

; Flush pipeline
jmp gdt.code:start_long_mode

gdt:
dq 0x0000000000000000

.code equ $ - gdt
dq 0x0020980000000000

.data equ $ - gdt
dq 0x0000900000000000

.pointer:
; Limit
dw $ - gdt - 1
; Size
dq gdt

[BITS 64]

start_long_mode:

; Disable interrupts
cli

; Set up stack
mov rbp, STACK_BASE
mov rsp, rbp

call KERNEL_OFFSET

; Fill boot sector with zeroes
times 510 - ($ - $$) db 0

; Bootloader signature
dw 0xaa55
